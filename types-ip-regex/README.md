This is a stub types definition for ip-regex (https://github.com/sindresorhus/ip-regex).

ip-regex provides its own type definitions, so you don't need @types/ip-regex installed!